<?php

use Illuminate\Database\Seeder;

class PlacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('places')->insert([
            'location_name'=>'Disneyland',
            'location_email'=>'disneyland@disneyland.com',
            'location_address'=>'Test Location',
            'location_phone'=>'123456789',
            'lat'=>'28.3852',
            'long'=>'81.5639',
            'imageurl'=>'1522087672.jpg',
            'imageurltwo'=>'1522087672.jpg',
            'opening_hours'=>'10:00 AM-10:00 PM',
            'verified'=>1
        ]);
        DB::table('places')->insert([
            'location_name'=>'Airport',
            'location_email'=>'airport@disneyland.com',
            'location_address'=>'Test Location',
            'location_phone'=>'123456789',
            'lat'=>'28.3852',
            'long'=>'81.5639',
            'imageurl'=>'1522087672.jpg',
            'imageurltwo'=>'1522087672.jpg',
            'opening_hours'=>'10:00 AM-10:00 PM',
            'verified'=>0
        ]);
    }
}
