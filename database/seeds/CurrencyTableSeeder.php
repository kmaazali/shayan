<?php

use Illuminate\Database\Seeder;
use App\Currency;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::create(['currency_name'=>'EUR']);
        Currency::create(['currency_name'=>'GBP']);
        Currency::create(['currency_name'=>'CHF']);
        Currency::create(['currency_name'=>'USD']);
        Currency::create(['currency_name'=>'AUD']);
        Currency::create(['currency_name'=>'CAD']);
    }
}
