<?php

use Illuminate\Database\Seeder;
use App\CurrencyPlace;

class ExchangeCurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table("currency_place")->insert([
            'currency_id'=>1,'place_id'=>1,'buy'=>'102.06','sell'=>'104.32'
        ]);
        DB::table("currency_place")->insert([
            'currency_id'=>2,'place_id'=>1,'buy'=>'109.06','sell'=>'112.32'
        ]);
        //CurrencyPlace::create(['currency_id'=>1,'place_id'=>1,'buy'=>'102.06','sell'=>'104.32']);
        //CurrencyPlace::create(['currency_id'=>2,'place_id'=>1,'buy'=>'109.06','sell'=>'112.32']);
    }
}
