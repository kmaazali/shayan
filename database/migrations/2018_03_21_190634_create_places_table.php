<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->string('location_name');
            $table->string('location_email')->nullable();
            $table->string('location_address');
            $table->string('location_phone')->nullable();
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->string('imageurl')->nullable();
            $table->string('imageurltwo')->nullable();
            $table->string('opening_hours');
            $table->integer('verified')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
