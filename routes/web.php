<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('reset_password/{token}', ['as' => 'password.reset', function($token)
// {
//     // implement your reset password route here!
// }]);

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/',function () {
   return view('login');
});
Route::get('/admin/list',"AdminController@");
Route::post("/adminlogin", "AdminController@login");
Route::get('/admin/addplace',function(){
   return view('addPlace');
});
Route::post('admin/formadd','AdminController@addPlace');
Route::get('/logout',"AdminController@logout");

Route::get('/list/all','AdminController@postlogin');
Route::resource('currencies', 'CurrencyController');
Route::get('place/{id}','AdminController@place');
Route::get('edit/place/{id}','PlaceController@editPlace');
Route::get('approve/place/{id}','PlaceController@approvePlace');
Route::post('edit/submit','PlaceController@editplacesubmit');
Route::get('delete/place/{id}','PlaceController@deleteplace');
Route::get('delete/currency/{id}/{placeid}','PlaceController@deletecurrency');
Route::get('list/unverified',function (){
   return view('unverified');
});


