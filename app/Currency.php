<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{

    //
    protected $table='currency';

    protected $fillable=['currency_name'];
    //protected $with=['currencyRate'];
    public function currencyRate(){
        return $this->belongsToMany('App\Place','exchange_currency','currency_id','place_id');
    }
}
