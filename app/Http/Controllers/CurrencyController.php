<?php

namespace App\Http\Controllers;

use App\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Illuminate\Support\Facades\Redirect;
use View;


class CurrencyController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        $currency = Currency::all();

        // load the view and pass the nerds
        return View::make('currency.index')
            ->with('currency', $currency);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('currency.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $currency = new Currency;
        $currency->currency_name       = Input::get('currency_name');

        $currency->save();

        // redirect
        Session::flash('message', 'Successfully created currency!');
        return \Redirect::to('currencies');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $currency = Currency::find($id);

        // show the view and pass the nerd to it
        return View::make('currency.show')
            ->with('currency', $currency);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $currency = Currency::find($id);

        // show the edit form and pass the nerd
        return View::make('currency.edit')
            ->with('currency', $currency);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $currency = Currency::find($id);
        $currency->currency_name       = Input::get('currency_name');
        $currency->save();

        // redirect
        Session::flash('message', 'Successfully updated currency!');
        return Redirect::to('currencies');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $currency = Currency::find($id);
        $currency->delete();

        // redirect

        return Redirect::to('currencies');
        //
    }

    public function getallcurrencies(){
        return Currency::all();
    }

}

