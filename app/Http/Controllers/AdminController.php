<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Currency;
use App\ExchangeCurrency;
use App\Place;
use Illuminate\Http\Request;

use View;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
    }
    public function login(Request $request){
        $email=($request['email']);
        $password=($request['pass']);
        $admin=Admin::where('username',$email)->where('password',$password)->get();
        if($admin->isEmpty()){
            echo "<script>alert('Unable To Login Admin');</script>";
            return redirect()->back();
        }
        else{
            session_start();

            $_SESSION['key']=$email;
            $data=Place::all();

            return view('allPlaces',['data'=>$data]);
        }
    }
    public function postlogin(){
        $data=Place::all();

        return view('allPlaces',['data'=>$data]);
    }


    public function listallplaces(){

        $data=Place::get();

        $data=Place::where('verified',1)->with('placeCurrency')->get();

        //$currency=Currency::all();
        return response()->json(['data'=>$data]);

    }

    public function listallunverified(){
        $data=Place::where('verified',0)->with('placeCurrency')->get();

        //$currency=Currency::all();
        return response()->json(['data'=>$data]);
    }


    public function addPlace(Request $request)
    {
        if ($request->hasFile('input_imgtwo')) {

            $location_email = $request['location_email'];
            $location_name = $request['location_name'];
            $location_address = $request['location_address'];
            $location_phone = $request['location_phone'];
            $lat = $request['lat'];
            $long = $request['long'];
            $opening_hours = $request['opening_hours'];
            $photoName = time() . '.' . $request['input_img']->getClientOriginalExtension();

            /*
            talk the select file and move it public directory and make avatars
            folder if doesn't exsit then give it that unique name.
            */
            $request['input_img']->move(public_path('/places'), $photoName);
            $photoNametwo = time() . '.' . $request['input_imgtwo']->getClientOriginalExtension();

            /*
            talk the select file and move it public directory and make avatars
            folder if doesn't exsit then give it that unique name.
            */
            $request['input_imgtwo']->move(public_path('/places/2'), $photoNametwo);

            $place = Place::create([
                'location_name' => $location_name,
                'location_email' => $location_email,
                'location_address' => $location_address,
                'location_phone' => $location_phone,
                'lat' => $lat,
                'long' => $long,
                'imageurl' => $photoName,
                'imageurltwo' => $photoNametwo,
                'opening_hours' => $opening_hours,
                'verified' => 1

            ]);
            if ($request->has('currency_name')) {
                foreach ($request['currency_name'] as $item) {

                    ExchangeCurrency::create([
                        'currency_id' => $request['currency_name'][0],
                        'place_id' => $place->id,
                        'buy' => $request['buy'][0],
                        'sell' => $request['sell'][0]
                    ]);
                }
            }
            $data = Place::all();
            return view('allPlaces', ['data' => $data]);

        }
        else if($request->hasFile('input_img')){
            $location_email = $request['location_email'];
            $location_name = $request['location_name'];
            $location_address = $request['location_address'];
            $location_phone = $request['location_phone'];
            $lat = $request['lat'];
            $long = $request['long'];
            $opening_hours = $request['opening_hours'];
            $photoName = time() . '.' . $request['input_img']->getClientOriginalExtension();

            /*
            talk the select file and move it public directory and make avatars
            folder if doesn't exsit then give it that unique name.
            */
            $request['input_img']->move(public_path('/places'), $photoName);


            $place = Place::create([
                'location_name' => $location_name,
                'location_email' => $location_email,
                'location_address' => $location_address,
                'location_phone' => $location_phone,
                'lat' => $lat,
                'long' => $long,
                'imageurl' => $photoName,
                'opening_hours' => $opening_hours,
                'verified' => 1

            ]);
            if ($request->has('currency_name')) {
                foreach ($request['currency_name'] as $item) {

                    ExchangeCurrency::create([
                        'currency_id' => $request['currency_name'][0],
                        'place_id' => $place->id,
                        'buy' => $request['buy'][0],
                        'sell' => $request['sell'][0]
                    ]);
                }
            }
            $data = Place::all();
            return view('allPlaces', ['data' => $data]);
        }
        else{
            $location_email = $request['location_email'];
            $location_name = $request['location_name'];
            $location_address = $request['location_address'];
            $location_phone = $request['location_phone'];
            $lat = $request['lat'];
            $long = $request['long'];
            $opening_hours = $request['opening_hours'];



            $place = Place::create([
                'location_name' => $location_name,
                'location_email' => $location_email,
                'location_address' => $location_address,
                'location_phone' => $location_phone,
                'lat' => $lat,
                'long' => $long,
                'opening_hours' => $opening_hours,
                'verified' => 1

            ]);
            if ($request->has('currency_name')) {
                foreach ($request['currency_name'] as $item) {

                    ExchangeCurrency::create([
                        'currency_id' => $request['currency_name'][0],
                        'place_id' => $place->id,
                        'buy' => $request['buy'][0],
                        'sell' => $request['sell'][0]
                    ]);
                }
            }
            $data = Place::all();
            return view('allPlaces', ['data' => $data]);
        }
    }

    public function logout(){
        session_start();
        session_destroy ();
        return view('login');
    }


    public function getAllCurrencies(){
        $data=Currency::all();
        return view('allCurrencies',['data'=>$data]);
    }


    public function place($id){
        $place = Place::find($id);


        // show the view and pass the nerd to it
        return view('showPlace',['place'=>$place]);
    }







}
