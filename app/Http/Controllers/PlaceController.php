<?php

namespace App\Http\Controllers;

use App\ExchangeCurrency;
use App\Place;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use DB;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use Symfony\Component\Debug\ExceptionHandler;
use Redirect;
use View;

class PlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function show(Place $place)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function edit(Place $place)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Place $place)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function destroy(Place $place)
    {
        //
    }

    public function sendEmail(Request $request){
        if($request->has('name')&&$request->has('email')&&$request->has('location_name')&&$request->has('location_address')&&$request->has('location_phone')){
            $name=$request['name'];
            $email=$request['email'];
            $location_name=$request['location_name'];
            $location_address=$request['location_address'];
            $location_phone=$request['location_phone'];

            Mail::send('emails.addplace', ['name' => $name, 'email' => $email,'location_name'=>$location_name,'location_address'=>$location_address,'location_phone'=>$location_phone], function ($message)
            {

                $message->from('me@gmail.com', 'Christian Nwamba');

                $message->to('k.maaz.ali@gmail.com');

            });
            return response()->json(['success'=>true,'message'=>'Mail Sent To Admin Successfully','array'=>['name'=>$name,'email'=>$email,'location_name'=>$location_name,'location_address'=>$location_address,'location_phone'=>$location_phone]],200);
        }
        else{
            return response()->json(['success'=>false,'message'=>'Please Fill In Required Fields'],200);
        }
    }


    public function addPlaces(Request $request){



        if($request->has('location_name')&&$request->has('location_email')&&$request->has('location_address')&&$request->has('location_phone')&&$request->has('lat')&&$request->has('long')&&$request->has('opening_hours')) {
            if($request->hasFile('imageurl')) {
                if ($request->hasFile('imageurltwo')) {
                    $photoName = time() . '.' . $request['imageurl']->getClientOriginalExtension();

                    /*
                    talk the select file and move it public directory and make avatars
                    folder if doesn't exsit then give it that unique name.
                    */
                    $request['imageurl']->move(public_path('/places'), $photoName);
                    $photoNametwo = time() . '.' . $request['imageurltwo']->getClientOriginalExtension();

                    /*
                    talk the select file and move it public directory and make avatars
                    folder if doesn't exsit then give it that unique name.
                    */
                    $request['imageurltwo']->move(public_path('/places/2'), $photoNametwo);
                    $place = Place::create([
                        'location_name' => $request['location_name'],
                        'location_email' => $request['location_email'],
                        'location_address' => $request['location_address'],
                        'location_phone' => $request['location_phone'],
                        'lat' => $request['lat'],
                        'long' => $request['long'],
                        'opening_hours' => $request['opening_hours'],
                        'imageurl' => $photoName,
                        'imageurltwo' => $photoNametwo,
                        'verified' => 0

                    ]);

                    if ($request->has('data')) {

                        $WorkingArray = json_decode(json_encode($request['data'], true), true);


                        foreach ($WorkingArray as $item) {

                            $item = json_decode($item, true);
                            ExchangeCurrency::create([

                                'buy' => $item['buy'],
                                'place_id' => $place->id,
                                'sell' => $item["sell"],
                                'currency_id' => $item["currency_id"]
                            ]);
                        }
                    }
//            dd($request->data);
//            exit();
//            if($request->has('currency_items')){
//                $allintests = [];
//                foreach($request['currency_items'] as $item){
//
//                    $allintests[]=new ExchangeCurrency();
//
//                    $allintests->currency_id=$item;
//                    $allintests->place_id=$place->id;
//                    $allintests->buy=$request->currency_buy;
//                    $allintests->sell=$request->currency_sell;
//
//
////                    ExchangeCurrency::create([
////                        'currency_id'=>$item,
////                        'place_id'=>$place->id,
////                        'buy'=>$request['currency_buy'],
////                        'sell'=>$request['currency_sell'],
////                    ]);
//                }
//
//                ExchangeCurrency::insert($allintests);
//
//            }
                    return response()->json(['success' => true, 'array' => $place], 200);
                }
                else{
                    $photoName = time() . '.' . $request['imageurl']->getClientOriginalExtension();

                    /*
                    talk the select file and move it public directory and make avatars
                    folder if doesn't exsit then give it that unique name.
                    */
                    $request['imageurl']->move(public_path('/places'), $photoName);

                    $place = Place::create([
                        'location_name' => $request['location_name'],
                        'location_email' => $request['location_email'],
                        'location_address' => $request['location_address'],
                        'location_phone' => $request['location_phone'],
                        'lat' => $request['lat'],
                        'long' => $request['long'],
                        'opening_hours' => $request['opening_hours'],
                        'imageurl' => $photoName,
                        'imageurltwo' => '',
                        'verified' => 0

                    ]);

                    if ($request->has('data')) {

                        $WorkingArray = json_decode(json_encode($request['data'], true), true);


                        foreach ($WorkingArray as $item) {

                            $item = json_decode($item, true);
                            ExchangeCurrency::create([

                                'buy' => $item['buy'],
                                'place_id' => $place->id,
                                'sell' => $item["sell"],
                                'currency_id' => $item["currency_id"]
                            ]);
                        }
                    }
//            dd($request->data);
//            exit();
//            if($request->has('currency_items')){
//                $allintests = [];
//                foreach($request['currency_items'] as $item){
//
//                    $allintests[]=new ExchangeCurrency();
//
//                    $allintests->currency_id=$item;
//                    $allintests->place_id=$place->id;
//                    $allintests->buy=$request->currency_buy;
//                    $allintests->sell=$request->currency_sell;
//
//
////                    ExchangeCurrency::create([
////                        'currency_id'=>$item,
////                        'place_id'=>$place->id,
////                        'buy'=>$request['currency_buy'],
////                        'sell'=>$request['currency_sell'],
////                    ]);
//                }
//
//                ExchangeCurrency::insert($allintests);
//
//            }
                    return response()->json(['success' => true, 'array' => $place], 200);
                }
            }
            else{


                $place = Place::create([
                    'location_name' => $request['location_name'],
                    'location_email' => $request['location_email'],
                    'location_address' => $request['location_address'],
                    'location_phone' => $request['location_phone'],
                    'lat' => $request['lat'],
                    'long' => $request['long'],
                    'opening_hours' => $request['opening_hours'],
                    'imageurl' => '',
                    'imageurltwo' => '',
                    'verified' => 0

                ]);

                if ($request->has('data')) {

                    $WorkingArray = json_decode(json_encode($request['data'], true), true);


                    foreach ($WorkingArray as $item) {

                        $item = json_decode($item, true);
                        ExchangeCurrency::create([

                            'buy' => $item['buy'],
                            'place_id' => $place->id,
                            'sell' => $item["sell"],
                            'currency_id' => $item["currency_id"]
                        ]);
                    }
                }
//            dd($request->data);
//            exit();
//            if($request->has('currency_items')){
//                $allintests = [];
//                foreach($request['currency_items'] as $item){
//
//                    $allintests[]=new ExchangeCurrency();
//
//                    $allintests->currency_id=$item;
//                    $allintests->place_id=$place->id;
//                    $allintests->buy=$request->currency_buy;
//                    $allintests->sell=$request->currency_sell;
//
//
////                    ExchangeCurrency::create([
////                        'currency_id'=>$item,
////                        'place_id'=>$place->id,
////                        'buy'=>$request['currency_buy'],
////                        'sell'=>$request['currency_sell'],
////                    ]);
//                }
//
//                ExchangeCurrency::insert($allintests);
//
//            }
                return response()->json(['success' => true, 'array' => $place], 200);
            }
        }
        else{
            return response()->json(['success'=>false,'message'=>'Please Fill In All Required Fields'],200);
        }

    }
    public function editPlace($id){
        $place=Place::where('id',$id)->get();
        $currency=ExchangeCurrency::where('place_id',$id)->with('currencyName')->get();


        return view('editplace',['place'=>$place,'currency'=>$currency]);
    }

    public function approvePlace($id){
        $place=Place::where('id',$id)->update(['verified' => 1]);



        return view('unverified');
    }

    public function editplacesubmit(Request $request){

        if($request->has('input_img')&&$request->has('input_imgtwo')&&$request['input_img']!=''&&$request['input_imgtwo']!=''){

            $photoName = time().'.'.$request['input_img']->getClientOriginalExtension();

            /*
            talk the select file and move it public directory and make avatars
            folder if doesn't exsit then give it that unique name.
            */
            $request['input_img']->move(public_path('/places'), $photoName);
            $photoNametwo = time().'.'.$request['input_imgtwo']->getClientOriginalExtension();

            /*
            talk the select file and move it public directory and make avatars
            folder if doesn't exsit then give it that unique name.
            */
            $request['input_imgtwo']->move(public_path('/places/2/'), $photoNametwo);
            Place::where('id',$request['placeid'])->update([
                'location_name'=>$request['location_name'],
                'location_email'=>$request['location_email'],
                'location_address'=>$request['location_address'],
                'location_phone'=>$request['location_phone'],
                'lat'=>$request['lat'],
                'long'=>$request['long'],
                'opening_hours'=>$request['opening_hours'],
                'imageurl'=>$photoName,
                'imageurltwo'=>$photoNametwo


            ]);
//            return view('allPlaces');
        }
        else {

            Place::where('id', $request['placeid'])->update([
                'location_name' => $request['location_name'],
                'location_email' => $request['location_email'],
                'location_address' => $request['location_address'],
                'location_phone' => $request['location_phone'],
                'lat' => $request['lat'],
                'long' => $request['long'],
                'opening_hours' => $request['opening_hours'],


            ]);;

        }
        for($i=0;$i<count($request['currency_id']);$i++){
            ExchangeCurrency::where('currency_id',$request['currency_id'][$i])->where('place_id',$request['placeid'])->update([

                'buy'=>$request['buy'][$i],
                'sell'=>$request['sell'][$i]
            ]);
        }
        if($request->has('currency_name_update')){

            for($i=0;$i<count($request['currency_name_update']);$i++){
                $exccurrency=ExchangeCurrency::where('place_id',$request->placeid)->where('currency_id',$request['currency_name_update'][$i])->get();
                if (!$exccurrency->isEmpty()) {
                    return redirect()->back()->with('alert', 'Unable To Process Duplicate Currency!');
                }
                else {
                    ExchangeCurrency::create([
                        'buy' => $request['buy_update'][$i],
                        'place_id' => $request->placeid,
                        'sell' => $request["sell_buy"][$i],
                        'currency_id' => $request["currency_name_update"][$i]
                    ]);
                    return \View::make('allPlaces');
                }
            }
        }
        return \View::make('allPlaces');

    }
    public function deleteplace($id){
        Place::where('id',$id)->delete();
        ExchangeCurrency::where('place_id',$id)->delete();
        return view('allPlaces');

    }
    public function deletecurrency($id,$placeid){

        $exd=ExchangeCurrency::where('currency_id',$id)->where('place_id',$placeid)->delete();

        return view('allPlaces');
    }
    function haverSine($latitude1, $longitude1, $latitude2, $longitude2) {

        $earth_radius = 6371;

        $dLat = deg2rad($latitude2 - $latitude1);
        $dLon = deg2rad($longitude2 - $longitude1);

        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
        $c = 2 * asin(sqrt($a));
        $d = $earth_radius * $c;
        return $d;

    }

    public function getSortedPlaces(Request $request)
    {

        $circle_radius = 3959;
        $aqi_near_by["aqi_near_by"] = [];
        $max_distance = $request['distance'];
        $lat = $request['lat'];
        $lon = $request['long'];
        $verified = 1;
        $place=Place::select(
            DB::raw("*, ROUND( 3959 * acos( cos( radians(?) ) * cos( radians( places.lat ) ) * cos( radians( places.long ) - radians(?) ) + sin( radians(?) ) * sin( radians( places.lat ) ) ) ,2) AS distance "))
            ->where('places.verified','=',"?")
//                ->groupBy('places.id')
            ->having("distance", "<", "?")
            ->orderBy("distance")
            ->with('placeCurrency')
            ->setBindings([$lat, $lon, $lat,$verified,  $max_distance])
            ->get();

//        //        $exchange=ExchangeCurrency::with('placeName')->with('currencyName')->get();
        foreach ($place as $item) {

            $lat = $item['lat'];
            $long = $item['long'];
            $userlat = $request['lat'];
            $userlong = $request['long'];
            //return $lat;
            $item['distance'] = round($this->haverSine($userlat, $userlong, $lat, $long),2);


        }
        $notificationsNew = $place->filter(function ($exchange) {
            if ($exchange->placeName['verified'] == 1) {
                return true;
            }
        });

        $notificationsNew = $notificationsNew->values()->all();
//        foreach($place as $item){
//            echo $item['distance'];
//        }
        return response()->json(['success' => true, 'places' => $place], 200);


    }




//    function haverSine($latitude1, $longitude1, $latitude2, $longitude2) {
//
//        $earth_radius = 6371;
//
//        $dLat = deg2rad($latitude2 - $latitude1);
//        $dLon = deg2rad($longitude2 - $longitude1);
//
//        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
//        $c = 2 * asin(sqrt($a));
//        $d = $earth_radius * $c;
//        return $d;
//
//    }

    public function sortCurrency(Request $request){
        $newcol=[];
        if($request->has('currency_id')&&$request->has('order')&&$request->has('buysell')){
            if($request['buysell']==0){
                $exchange=ExchangeCurrency::where('currency_id',$request['currency_id'])->orderBy('buy',$request['order'])->with('placeName')->with('currencyName')->get();
//                return $exchange[0]['place_name']['lat'];
//                for($i=0;$i<$exchange->count();$i++){
//                    $lat=$exchange[$i]->place_name['lat'];
//                    $long=$exchange[$i]['place_name']['long'];
//                    $userlat=$request['lat'];
//                    $userlong=$request['long'];
//                    return $lat;
//                    $exchange[$i]['distance']=$this->haverSine($userlat,$userlong,$lat,$long,$earthRadius = 6371000);
//
//
//                }

                foreach($exchange as $item){

                    $lat = $item->placeName['lat'];
                    $long = $item->placeName['long'];
                    $userlat = $request['lat'];
                    $userlong = $request['long'];
                    //return $lat;
                    $item['distance'] = round($this->haverSine($userlat, $userlong, $lat, $long),2);


                }
                $notificationsNew = $exchange->filter(function($exchange)
                {
                    if($exchange->placeName['verified'] == 1) {
                        return true;
                    }
                });
                $notificationsNew=$notificationsNew->values()->all();





                return response()->json(['success'=>true,'place'=>$notificationsNew],200);
            }
            else{
                $exchange=ExchangeCurrency::where('currency_id',$request['currency_id'])->orderBy('sell',$request['order'])->with('placeName')->with('currencyName')->get();

                foreach($exchange as $item){

                    $lat=$item->placeName['lat'];
                    $long=$item->placeName['long'];
                    $userlat=$request['lat'];
                    $userlong=$request['long'];

                    $item['distance'] = round($this->haverSine($userlat, $userlong, $lat, $long),2);
                }
                $notificationsNew = $exchange->filter(function($exchange)
                {
                    if($exchange->placeName['verified'] == 1) {
                        return true;
                    }
                });

//                for($i=0;$i<$exchange->count();$i++){
//                    $lat=$exchange[$i]['lat'];
//                    $long=$exchange[$i]['long'];
//                    $userlat=$request['lat'];
//                    $userlong=$request['long'];
//
//                    $exchange[$i]['distance']=$this->haverSine($lat,$long,$userlat,$userlong,$earthRadius = 6371000);
//
//                }
                $notificationsNew=$notificationsNew->values()->all();
                return response()->json(['success'=>true,'place'=>$notificationsNew],200);
            }

        }
        else{
            return response()->json(['success'=>false,'message'=>'Please Fill In The Required Fields']);
        }
    }

}