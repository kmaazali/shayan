<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    //
   //
    protected $table='places';
    protected $fillable = ['name', 'location_email', 'location_name','location_address','location_phone','lat','long','imageurl','imageurltwo','opening_hours','verified'];

    //protected $with=['placeCurrency'];
    public function placeCurrency(){
        //return $this->belongsToMany('App\Currency','exchange_currency','place_id','currency_id');
        //return $this->belongsToMany('App\Currency')->withPivot('buy,sell');
        return $this->belongsToMany('App\Currency')->withPivot('buy', 'sell');
    }

}
