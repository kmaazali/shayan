<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ExchangeCurrency extends Model
{
    //

    protected $table='currency_place';

    protected $fillable=[
        'currency_id',
        'place_id',
        'buy',
        'sell'
    ];
    public function currencyName(){
        return $this->belongsTo('App\Currency','currency_id','id');
    }
    public function placeName(){
        return $this->belongsTo('App\Place','place_id','id');
    }



}
