<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
    <script src="http://34.244.154.76/js/lightbox.js"></script>
    <link rel="stylesheet" href="http://34.244.154.76/css/lightbox.css">
    <style>

        body, html, .container-fluid {
            color:white;
            margin:0;
            height: auto;
            background: linear-gradient(#1891db, #db6218);
            background-image: -webkit-linear-gradient(bottom, #1891db 0%, #db6218 100%);
            background-repeat: no-repeat;
        }

        table{
            color:black;
        }
        a{
            color:white;
        }
        a:visited{
            color:white;
        }
    </style>
    <script>
        /* Formatting function for row details - modify as you need */
        function format ( d, $this ) {
            // `d` is the original data object for the row
            //           var data = $.each(d.place_currency, function( index, value ) {

            return $.each(d.place_currency, function( index, value ) {
                alert(value);


                //                var row = document.getElementById("");
                //
                //                var cell = document.createElement("td");
                //                var cellText = document.createTextNode("Currency Name : "+ value.currency_name);
                //                cell.appendChild(cellText);
                //                row.appendChild(cell);
                //                row.appendChild(cell);

                //                var row = document.getElementById("elementRow");
                //                var cell1 = row.insertCell(0);
                //            cell1.innerHTML = value.currency_name +'Curency Value ' + value.pivot.sell;



                //             '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
                //   var li =  '<tr>'+
                //     '<td>Full name:</td>'+
                //     '<td>'+d.currency_name+'</td>'+
                //     '</tr>'+
                //   '<tr>'+
                //     '<td>Extension number:</td>'+
                //     '<td>'+d.id+'</td>'+
                //     '</tr>'+
                //   '<tr>'+
                //     '<td>Extra info:</td>'+
                //     '<td>And any further details here (images etc)...</td>'+
                //     '</tr>';
                //li.append('<table></table>');
                //                    '</table>';

            });

        }




    </script>

    <title>
        Places Dashboard
    </title>
</head>
<body>
<div class="container-fluid">
    <div class="row">

        <nav class="navbar navbar-inverse">
            <ul class="nav navbar-nav">
                <li><a href="#">Exchange Companion</a></li>
                <li><a href="/list/all">All Places</a></li>
                <li><a href="/admin/addplace">Add Places</a></li>
                <li><a href="{{ URL::to('currencies') }}">All Currencies</a></li>
                <li><a href="{{ URL::to('currencies/create') }}">Create a Currency</a>
                <li><a href="{{ URL::to('list/unverified') }}">Unverified Places</a>
                <li><a href="/logout">Logout</a></li>
            </ul>

        </nav>






        <div class="container">
            <h2>All Places</h2>
            <div class="table-responsive">
                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th></th>
                        <th>ID</th>
                        <th>Location Name</th>
                        <th>Location Email</th>
                        <th>Location Address</th>
                        <th>Location Phone</th>
                        <th>Opening Hours</th>
                        <th>Image One</th>
                        <th>Image Two</th>

                    </tr>
                    </thead>



                </table>
            </div>

            <div id="slipknot" style="display:none;"></div>

        </div>
    </div>
</div>


<script>



    $(document).ready(function() {
        var table = $('#example').DataTable({
            'ajax': 'http://34.244.154.76/api/list/all/unverified',
            'columns': [
                {
                    'className': 'details-control',
                    'orderable': false,
                    'data': null,
                    'defaultContent': ''
                },
                {'data': 'id'},
                {'data': 'location_name'},
                {'data': 'location_email'},
                {'data': 'location_address'},
                {'data': 'location_phone'},
                {'data': 'opening_hours'},
                {
                    "render": function (data, type, JsonResultRow, meta) {
                        return '<a  data-lightbox="image-1" style="color:black;" href="http://34.244.154.76/places/'+JsonResultRow.imageurl+'">Click Here</a>';
                    }
                },
                {
                    "render": function (data, type, JsonResultRow, meta) {
                        return '<a  data-lightbox="image-2" style="color:black;" href="http://34.244.154.76/places/2/'+JsonResultRow.imageurltwo+'">Click Here</a>';
                    }
                },


            ],
            'order': [[1, 'asc']],

        });
        $(".edit-field").on('click',function () {
            alert('done');
        });

        $("#example").on('click','tr',function(e) {

            var tr = $(this).closest('tr');
            var row = table.row( tr );
            if($('#slipknot').is(':hidden')){
                $("#slipknot").show();
                var x=$(':nth-child(2)', this).html();
                console.log(x);
                var y='/approve/place/';
                var z='/delete/place/';


                $(this).append("<div class='parent'><a class='edit-link'><p style='color:black !important;' class='edit-field'>Approve</p></a><a class='delete-link'><p style='color:black !important;'>Delete</p></a></div>");
                $('.edit-link').attr('href',y+x);
                $('.delete-link').attr('href',z+x);
                $.each(row.data().place_currency, function (index, value) {
                    //alert(value.currency_name);

                    tr.after("<tr class='myrow'><th>Currency</th><th>Buy</th><th>Sell</th></tr><tr class='myrow'><td>" + value.currency_name + "</td><td>" + value.pivot.buy + "</td><td>" + value.pivot.sell + "</td></tr>");
                    $("#slipknot").show();
                    // $(this).after("<a href='/edit/place/'+value.currency_id><p>Edit</p></a>");

                    //$("<table><tr><th>Currency</th></tr><tr><td>"+value.currency_name+"</td></tr></table>").appendTo(row);
                });
            }
            else{
                $('.myrow').css('display','none');
                $("#slipknot").hide();
                $(".edit-field").hide();
                $('.delete-link').hide();
            }


            //console.log(row.data());
            //console.log(row.data().place_currency);
            // $.each(d.place_currency, function (index, value) {
            //   alert(value);
            // });
        });
        // $("tr").click(function () {
        // $(this).parent("td").append("<table></table>");
        // // Open this row
        // row.child(format(row.data(), $(this))).show();
        // tr.addClass('shown');
        //$.each(d.place_currency, function( index, value ) {
        //       alert(value);
        //});
        // });
    });

</script>
</body>
</html>




{{--<html>--}}
{{--<head>--}}
{{--<title>Datatable</title>--}}
{{--<link href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css" type="text/css">--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>--}}
{{--<script src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>--}}
{{--<style>--}}
{{--td.details-control {--}}
{{--background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_open.png') no-repeat center center;--}}
{{--cursor: pointer;--}}
{{--}--}}
{{--tr.shown td.details-control {--}}
{{--background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_close.png') no-repeat center center;--}}
{{--}--}}
{{--</style>--}}
{{--<script>--}}
{{--/* Formatting function for row details - modify as you need */--}}
{{--function format ( d, $this ) {--}}
{{--// `d` is the original data object for the row--}}
{{--//           var data = $.each(d.place_currency, function( index, value ) {--}}

{{--$.each(d.place_currency, function( index, value ) {--}}
{{--//alert(value.pivot.buy);--}}

{{--var row="<table><tr><th>Currency Name</th><th>Buy Rate</th><th>Sell Rate</th></tr><tr><td>"+value.currency_name+"</td><td>"+value.pivot.buy+"</td><td>"+value.pivot.sell+"</td></tr></table>";--}}
{{--$($this).append(row);--}}

{{--//                var row = document.getElementById("");--}}
{{--//--}}
{{--//                var cell = document.createElement("td");--}}
{{--//                var cellText = document.createTextNode("Currency Name : "+ value.currency_name);--}}
{{--//                cell.appendChild(cellText);--}}
{{--//                row.appendChild(cell);--}}
{{--//                row.appendChild(cell);--}}

{{--//                var row = document.getElementById("elementRow");--}}
{{--//                var cell1 = row.insertCell(0);--}}
{{--//            cell1.innerHTML = value.currency_name +'Curency Value ' + value.pivot.sell;--}}



{{--//             '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+--}}

{{--//                    '</table>';--}}

{{--});--}}

{{--}--}}

{{--$(document).ready(function() {--}}
{{--var table = $('#example').DataTable({--}}
{{--'ajax': 'http://34.244.154.76/api/list/all/places',--}}
{{--'columns': [--}}
{{--{--}}
{{--'className':      'details-control',--}}
{{--'orderable':      false,--}}
{{--'data':           null,--}}
{{--'defaultContent': ''--}}
{{--},--}}
{{--{ 'data': 'location_name' },--}}
{{--{ 'data': 'location_email' },--}}
{{--{ 'data': 'location_address' }--}}
{{--],--}}
{{--'order': [[1, 'asc']]--}}
{{--} );--}}

{{--// Add event listener for opening and closing details--}}
{{--$('#example tbody').on('click', 'td.details-control', function(){--}}
{{--var tr = $(this).closest('tr');--}}
{{--var row = table.row( tr );--}}

{{--if(row.child.isShown()){--}}

{{--// This row is already open - close it--}}
{{--row.child.hide();--}}
{{--tr.removeClass('shown');--}}
{{--} else {--}}
{{--// $(this).parent("tr").append("<table></table>");--}}
{{--// Open this row--}}
{{--row.child(format(row.data(), $(this))).show();--}}
{{--tr.addClass('shown');--}}
{{--}--}}
{{--});--}}

{{--// Handle click on "Expand All" button--}}
{{--$('td').on('click', function(){--}}
{{--// Enumerate all rows--}}
{{--table.rows().every(function(){--}}
{{--// If row has details collapsed--}}
{{--if(!this.child.isShown()){--}}
{{--// Open this row--}}
{{--this.child(format(this.data())).show();--}}
{{--$(this.node()).addClass('shown');--}}
{{--}--}}
{{--});--}}
{{--});--}}

{{----}}
{{--});--}}
{{--</script>--}}
{{--</head>--}}
{{--<body>--}}

{{--<div>--}}
{{--<h3><a target="_blank" href="https://www.gyrocode.com/articles/jquery-datatables-how-to-expand-collapse-all-child-rows/">jQuery DataTables: How to expand/collapse all child rows</a> <small>Regular table</small></h3>--}}
{{--<button id="btn-show-all-children" type="button">Expand All</button>--}}
{{--<button id="btn-hide-all-children" type="button">Collapse All</button>--}}
{{--<hr>--}}
{{--<table id="example" class="display" cellspacing="0" width="100%">--}}
{{--<thead>--}}
{{--<tr id="elementRow">--}}

{{--<th>Expand/Collapse</th>--}}
{{--<th>Name</th>--}}
{{--<th>Location</th>--}}
{{--<th>Address</th>--}}
{{--</tr>--}}
{{--</thead>--}}
{{--<tfoot>--}}
{{--<tr >--}}
{{--<th>Expand/Collapse</th>--}}
{{--<th>Name</th>--}}
{{--<th>Location</th>--}}
{{--<th>Address</th>--}}
{{--</tr>--}}
{{--</tfoot>--}}
{{--</table>--}}
{{--</div>--}}


{{--</body>--}}
{{--</html>--}}