<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="/js/lightbox.js"></script>
    <link rel="stylesheet" href="/css/lightbox.css">
    <style>
        body, html, .container-fluid {
            color:white;
            margin:0;
            height: 100%;
            background: linear-gradient(#1891db, #db6218);
            background-image: -webkit-linear-gradient(bottom, #1891db 0%, #db6218 100%);
            background-repeat: no-repeat;
        }
        a{
            color:white;
        }
        a:visited{
            color:white;
        }
    </style>

    <title>
        Currencies Dashboard
    </title>
</head>
<body>
<div class="container-fluid">
    <div class="row">

        <nav class="navbar navbar-inverse">
            <ul class="nav navbar-nav">
                <li><a href="#">Exchange Companion</a></li>
                <li><a href="/list/all">All Places</a></li>
                <li><a href="/admin/addplace">Add Places</a></li>
                <li><a href="{{ URL::to('currencies') }}">All Currencies</a></li>
                <li><a href="{{ URL::to('currencies/create') }}">Create a Currency</a>
                <li><a href="{{ URL::to('list/unverified') }}">Unverified Places</a>
                <li><a href="/logout">Logout</a></li>
            </ul>

        </nav>
        <div class="container">
            <h2>All Places</h2>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Currency Name</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($data as $currency)
                        <tr>
                            <td>{{ $currency->id }}</td>
                            <td>{{ $currency->currency_name }}</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>



</body>
</html>
