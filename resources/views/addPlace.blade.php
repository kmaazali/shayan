<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>



     <script>
         var usedNames = [];
        function showdiv(){
            $(".showcurrencydiv").after('<br><div class="testdiv"><div class="form-group"><input class="remove btn btn-danger" type="button" value="Remove"/><br><label>Currency Name</label><select name="currency_name[]" class="currencyname form-control" required></select></div><div class="form-group"><label>Currency Buy Value</label><input class="form-control" type="text" value="" name="buy[]" required></div><div class="form-group"><label>Currency Sell Value</label><input class="form-control" type="text" value="" name="sell[]" required></div></div>\n' +
                '   ');
            getvals();
        }

        function remove(no){
            document.getElementById("preview"+no).value = "";
        }

        function getvals(){
            $.getJSON("http://34.244.154.76/api/get/all/currencies", function(data) {


                $.each(data, function(index, element) {
                    if (usedNames.indexOf(element.currency_name) == -1) {
                        //alert(element.currency_name);
                        $(".currencyname").append("<option value='" + element.id + "'>" + element.currency_name + "</option>");
                        usedNames.push(index);
                        console.log(index);
                    }
                });

            });
        }


        $(document).ready(function() {


            $('input.timepicker').timepicker({});


            $(document).on('click', '.remove', function () {

                $(this).parent().parent().remove();
            });
        });


    </script>


    <style>
        body, html, .container-fluid {
            color:white;
            margin:0;
            background: linear-gradient(#1891db, #db6218);
            background-image: -webkit-linear-gradient(bottom, #1891db 0%, #db6218 100%);
            background-repeat: no-repeat;
        }
        .form-control{
            color:black;
        }
    </style>

    <title>
        Places Dashboard
    </title>
</head>
<body>
<div class="container-fluid">
    <div class="row">

        <nav class="navbar navbar-inverse">
            <ul class="nav navbar-nav">
                <li><a href="#">Exchange Companion</a></li>
                <li><a href="/list/all">All Places</a></li>
                <li><a href="/admin/addplace">Add Places</a></li>
                <li><a href="{{ URL::to('currencies') }}">All Currencies</a></li>
                <li><a href="{{ URL::to('currencies/create') }}">Create a Currency</a>
                <li><a href="{{ URL::to('list/unverified') }}">Unverified Places</a>
                <li><a href="/logout">Logout</a></li>
            </ul>

        </nav>
        <div class="container">
            <h2>All Places</h2>
            <form action="{{action('AdminController@addPlace')}}" method="post" class="login100-form validate-form" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">



                <div class="form-group">
                    <label class="control-label col-sm-2" for="location_name">Exchange Name:</label>
                    <input class="form-control" type="text" value="" placeholder="Location Name" name="location_name" required>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="location_name">Exchange Email:</label>
                    <input class="form-control" type="email" value="" placeholder="Location Email" name="location_email" required>
                </div>


                <div class="form-group">
                    <label class="control-label col-sm-2" for="location_address">Exchange Address:</label>
                    <input class="form-control" type="text" value="" placeholder="Location Address" name="location_address" required>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="location_phone">Exchange Phone:</label>
                    <input class="form-control" type="text" value="" placeholder="Location Phone" name="location_phone" required>
                </div>
                <div id="pac-container form-group">
                    <label class="control-label col-sm-2" for="Enter_location">Enter Location Here:</label>
                    <input id="pac-input" type="text" class="form-control"
                           placeholder="Enter a location">
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="lat">Latitude:</label>
                    <input class="form-control" id="lat" type="text" value="" placeholder="Latitude" name="lat" required>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="long">Longitude:</label>
                    <input class="form-control" id="long" type="text" value="" placeholder="Longitude" name="long" required>
                </div>
                <div id="type-selector" class="pac-controls">
                    <input type="radio" name="type" id="changetype-all" checked="checked" style="display:none;">
                    <label for="changetype-all" style="display:none;">All</label>

                    <input type="radio" name="type" id="changetype-establishment" style="display:none;">
                    <label for="changetype-establishment" style="display:none;">Establishments</label>

                    <input type="radio" name="type" id="changetype-address" style="display:none;">
                    <label for="changetype-address" style="display:none;">Addresses</label>

                    <input type="radio" name="type" id="changetype-geocode" style="display:none;">
                    <label for="changetype-geocode" style="display:none;">Geocodes</label>
                </div>
                <div id="strict-bounds-selector" class="pac-controls">
                    <input type="checkbox" id="use-strict-bounds" value="" style="display:none;">
                    <label for="use-strict-bounds" style="display:none;">Strict Bounds</label>
                </div>



    <div id="map"></div>
    <div id="infowindow-content">
        <img src="" width="16" height="16" id="place-icon">
        <span id="place-name"  class="title"></span><br>
        <span id="place-address"></span>
    </div>



                <div class="form-group">
                    <label for="imageInput">Upload Image For A Place</label>
                    <input data-preview="#preview" name="input_img" type="file" id="imageInput1" required>
                    <img class="col-sm-6" id="preview1"  src="" ><br>
                    <a href=""  style="color:white;" onclick="remove(1);return false;">Remove</a>

                </div>

                <div class="form-group">
                    <label for="imageInput2">Upload Image For A Place</label>
                    <input data-preview="#preview" name="input_imgtwo" type="file" id="imageInput2">
                    <img class="col-sm-6" id="preview2"  src="" ><br>
                    <a href=""  style="color:white;" onclick="remove(2);return false">Remove</a>

                </div>

                <div class="form-group">
                    <label for="starttime">Start Time</label>
                    <input class="form-control" type="text" value="" placeholder="Opening Hours" name="opening_hours">
                </div>



                <div class="form-group">
                    <button class="btn btn-primary addcurrencybtn" onclick="showdiv();return false;">Add Currencies</button>
                </div>
                <div class="showcurrencydiv"></div>



                <div class="form-group">
                    <input type="submit" class="btn btn-default" value="Add Entry">
                </div>

            </form>

        </div>
    </div>
</div>

<script>
    // function remove(this){
    //     alert(this);
    //     $(this).parent().parent().remove();
    // }

</script>
<script>
    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 13
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
          //  var place = autocomplete.getPlace();
            var place = autocomplete.getPlace();
// get lat
            var lat = place.geometry.location.lat();
// get lng
            var lng = place.geometry.location.lng();

            $("#lat").val(lat);
            $("#long").val(lng);
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");

            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent = address;
            infowindow.open(map, marker);
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
            var radioButton = document.getElementById(id);
            radioButton.addEventListener('click', function() {
                autocomplete.setTypes(types);
            });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);

        document.getElementById('use-strict-bounds')
            .addEventListener('click', function() {
                console.log('Checkbox clicked! New state=' + this.checked);
                autocomplete.setOptions({strictBounds: this.checked});
            });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjSr1XgPa0Tt4doEeIb-oWVEr0fLkRHwk&libraries=places&callback=initMap"
        async defer></script>
</body>
</html>
