<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="/js/lightbox.js"></script>
    <link rel="stylesheet" href="/css/lightbox.css">
    <style>
        body, html, .container-fluid {
            color:white;
            height: 100%;
            margin:0;
            background: linear-gradient(#1891db, #db6218);
            background-image: -webkit-linear-gradient(bottom, #1891db 0%, #db6218 100%);
            background-repeat: no-repeat;
        }
        a{
            color:white;
        }
        a:visited{
            color:white;
        }
    </style>

    <title>
        Currencies Dashboard
    </title>
</head>
<body>
<div class="container-fluid">
    <div class="row">

        <nav class="navbar navbar-inverse">
            <ul class="nav navbar-nav">
                <li><a href="#">Exchange Companion</a></li>
                <li><a href="/list/all">All Places</a></li>
                <li><a href="/admin/addplace">Add Places</a></li>
                <li><a href="{{ URL::to('currencies') }}">All Currencies</a></li>
                <li><a href="{{ URL::to('currencies/create') }}">Create a Currency</a>
                <li><a href="{{ URL::to('list/unverified') }}">Unverified Places</a>
                <li><a href="/logout">Logout</a></li>
            </ul>

        </nav>
        <div class="container">
            <h2>All Currencies</h2>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Currency Name</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($currency as $key => $value)
                        <tr>
                            <td>{{ $value->id }}</td>
                            <td>{{ $value->currency_name }}</td>
                            <td style="display:inline-flex;">
<form method="POST" action="{{ route('currencies.destroy', $value->id) }}">
    <input type="submit" class="btn btn-warning" value="Delete This Currency">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="_method" value="DELETE" />

</form>


        <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
        <!-- we will add this later since its a little more complicated than the other two buttons -->

        <!-- show the nerd (uses the show method found at GET /nerds/{id} -->


        <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
        <a class="btn btn-small btn-info" href="{{ URL::to('currencies/' . $value->id . '/edit') }}">Edit this Currency</a>

    </td>


</tr>
@endforeach
</tbody>
</table>
</div>

</div>
</div>
</div>



</body>
</html>
